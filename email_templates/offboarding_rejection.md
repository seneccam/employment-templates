When setting up rejection rules for offboarded team members, please use below templates;



## People Ops

Thank you for reaching out, the team member you are trying to contact is no longer with GitLab, please reach out to;

1. compensation@gitlab.com - For compensation and benefits query
2. recruiting@gitlab.com - For recruiting queries
3. ces@gitlab.com - For candidate-related queries
4. accountingops@gitlab.com - For accounting-related queries
5. finance@gitlab.com For finance-related queries
6. peopleops@gitlab.com - For Business Technology, legal and general people ops assistance or if unsure of which category to choose

If the issue is sensitive and you would prefer to reach out to an individual, please email the closest group and request to be redirected to the best person to contact.


## Sales


Thank you for reaching out, the team member you are trying to contact is no longer with GitLab, please reach out to;

1. sales-commercial@gitlab.com - For commercial sales queries
2. sales-cs@gitlab.com - For customer solutions or success queries
3. sales-ent@gitlab.com - For enterprise sales related queries
4. sales-fieldops@gitlab.com - For field operations related queries
5. sales-all@gitlab.com - For general sales assistance or if unsure of which category to choose

If the issue is sensitive and you would prefer to reach out to an individual, please email the closest group and request to be redirected to the best person to contact.

## Marketing


Thank you for reaching out, the team member you are trying to contact is no longer with GitLab, please reach out to;

1. fieldmarketing@gitlab.com - For field marketing queries
2. marketing@gitlab.com - For community relations, demand generation, digital marketing,  and general marketing assistance or if unsure of which category to choose

If the issue is sensitive and you would prefer to reach out to an individual, please email the closest group and request to be redirected to the best person to contact.


## Engineering

Thank you for reaching out, the team member you are trying to contact is no longer with GitLab, please reach out to;

1. supportteam@gitlab.com - For support-related queries
2. development@gitlab.com - For development queries
3. infrastructure@gitlab.com - For infrastructure queries
4. meltano@gitlab.com - for Meltano-related queries
5. quality@gitlab.com - For quality queries
6. ux-department@gitlab.com - For UX/ product design related queries
7. security-gl-team@gitlab.com - For security-related queries
8. engineering@gitlab.com - For general assistance or if unsure of which category to choose

If the issue is sensitive and you would prefer to reach out to an individual, please email the closest group and request to be redirected to the best person to contact.


##Other

Thank you for reaching out, the team member you are trying to contact is no longer with GitLab, please reach out to peopleops@gitlab.com for general assistance.
