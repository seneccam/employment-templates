### Day 1 - For Team Members in New Zealand only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please complete and sign the [IR330 form](https://www.ird.govt.nz/-/media/project/ir/home/documents/forms-and-guides/ir300---ir399/ir330/ir330-2019.pdf). 
2. [ ] New team member: Once completed, please upload in BambooHR under 'Employee Uploads' and make a comment in the issue and tag `sszepietowska` or `nprecilla` (Non US Payroll). 
3. [ ] New team member: Please review the [GitLab PTY LTD Remote Work Checklist](https://forms.gle/tT7gEDPFyWpkVfkS8). _Please complete this checklist within the first 30 days of your start date_. 
</details>

<details>
<summary>Legal</summary>

1. [ ] @tnix Review responses to remote work form and follow up with team member, if needed.
</details>
