#### Frontend Engineers

<details>
<summary>New Team Member</summary>

##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.
1. [ ] Review [the MR acceptance checklist](https://docs.gitlab.com/ee/development/code_review.html#acceptance-checklist) which is used to help prevent production outages

</details>

<details>
<summary>Manager</summary>

1. [ ] Create a Frontend Engineer Training Issue in the [Frontend Onboarding Issue Tracker](https://gitlab.com/gitlab-org/frontend/onboarding/issues). Use the `Frontend_onboarding.md` template. Assign the issue to yourself, the manager and their FE onboarding buddy.


</details>

